package com.example.demo;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class DemoController {

	private static final ThreadLocal<DemoDataHolder> DATA_CACHE = ThreadLocal.withInitial(DemoDataHolder::new);

	@Autowired
	private DemoRepository repository;

	@GetMapping(value = "/id/generate", produces = APPLICATION_JSON_VALUE)
	public Mono<DemoResponse> generate() {

		return Mono.defer(() -> {

			final DemoDataHolder data = DATA_CACHE.get();

			if (data.isEof()) {

				return repository.getNextIdRange().map(data::init);

			}

			return Mono.just(data);

		}).map(data -> new DemoResponse(data.incrementAndGet()));

	}

	@PreDestroy
	public void preDestroy() {

		DATA_CACHE.remove();

	}

}
