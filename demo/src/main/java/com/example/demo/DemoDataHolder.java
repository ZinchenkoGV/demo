package com.example.demo;

public class DemoDataHolder {

	private static final int INCREMENT = 1000000;

	private long value = 0;

	private long max = 0;

	public DemoDataHolder init(long idRange) {

		value = idRange * INCREMENT;

		max = value + INCREMENT;

		return this;

	}

	public long incrementAndGet() {

		return value++;

	}

	public boolean isEof() {

		return value == max;

	}

}
