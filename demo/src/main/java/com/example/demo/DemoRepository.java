package com.example.demo;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@Repository
public class DemoRepository {

	private static final String CREATE_SCHEMA_SQL = "CREATE SEQUENCE IF NOT EXISTS ID_RANGE_SEQ";

	private static final String SELECT_NEXT_ID_RANGE_SQL = "SELECT ID_RANGE_SEQ.nextval - 1 AS id FROM dual";

	@Autowired
	@Qualifier("jdbcScheduler")
	private Scheduler jdbcScheduler;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@PostConstruct
	public void postConstruct() {

		jdbcTemplate.execute(CREATE_SCHEMA_SQL);

	}

	public Mono<Long> getNextIdRange() {

		return Mono.fromCallable(() -> jdbcTemplate.queryForObject(SELECT_NEXT_ID_RANGE_SQL, Long.class))
				.subscribeOn(Schedulers.parallel()).publishOn(jdbcScheduler);

	}

}
