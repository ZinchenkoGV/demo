package com.example.demo;

public class DemoResponse {

	private final long id;

	public DemoResponse(long id) {

		this.id = id;

	}

	public long getId() {

		return id;

	}

}
