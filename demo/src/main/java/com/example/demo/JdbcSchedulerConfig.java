package com.example.demo;

import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@Configuration
public class JdbcSchedulerConfig {

	@Value("${spring.datasource.hikari.maximum-pool-size}")
	private int maximumPoolSize;

	@Bean
	public Scheduler jdbcScheduler() {

		return Schedulers.fromExecutor(Executors.newFixedThreadPool(maximumPoolSize));

	}

}
